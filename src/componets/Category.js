import { Avatar, Card, Grid, Stack, Typography } from '@mui/material'
import React from 'react'


const list = [
    {
        image: '/images/category/Rectangle19.svg',
        name: 'Basic Spices',
    },
    {
        image: '/images/category/Rectangle16.svg',
        name: 'Whole Spices',

    },
    {
        image: '/images/category/Rectangle17.svg',
        name: 'Blended Spices',

    },
    {
        image: '/images/category/Rectangle18.svg',
        name: 'Ready -Mix Spices',
    },
    {
        image: '/images/category/Rectangle20.svg',
        name: 'Organic Spices',
    },
    {
        image: '/images/category/Rectangle21.svg',
        name: 'Pasta & Vernicilli',
    },
]
function Category() {
    return (
        <>
            <Stack px={{ xs: 2, lg: 10 }} py={0} my={{xs:0,lg:-3}}>

                <Card sx={{
                    justifyContent: 'center', alignItems: 'center', display: 'flex', flexDirection: 'column',
                    p: { xs: 1, lg: 2 }, borderRadius: 4
                }}>
                    <Typography sx={{ fontSize:{xs:15,lg:19}, fontWeight: 'bold' }}>
                        Order from our categories
                    </Typography>
                    <Grid container spacing={2} mt={1}>
                        {
                            list.map((each) => {
                                return (
                                    <Grid item xs={6} lg={2}>
                                        <Card sx={{ borderRadius: 4 }}>
                                            <Stack sx={{
                                                position: 'relative',
                                                zIndex: 0,
                                            }}>
                                                <img src={each.image} />
                                                {/* <Stack alignItems={'center'} > */}
                                                <Typography sx={{
                                                    position: 'absolute', zIndex: 1, mt: { xs: 2, lg: 5 }, ml: 2, width: { xs: '70%', lg: '40%' }, fontSize: 15,
                                                    fontWeight: 'bold', color: '#fff'
                                                }}>
                                                    {each.name}
                                                </Typography>
                                                {/* </Stack> */}
                                            </Stack>
                                        </Card>
                                    </Grid>
                                )
                            }
                            )
                        }

                    </Grid>

                </Card>
            </Stack>

        </>
    )
}

export default Category