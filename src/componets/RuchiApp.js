import { Avatar, Box, Button, Grid, Hidden, Paper, Stack, Typography } from '@mui/material'
import InputBase from '@mui/material/InputBase';
import React from 'react'

function RuchiApp() {
    return (
        <>
        <Stack mt={{xs:3,lg:0}}>

            <Stack sx={{
                position: 'relative',
                zIndex: 0,
            }}>
                <Avatar variant='square' src='/images/android/Frame 30.svg' sx={{ height:{xs:'170vh' ,lg:'100vh'}, width: 'auto' }} />
                <Stack sx={{
                    position: 'absolute',
                    zIndex: 1,
                }}>
                    <Grid container spacing={2}>
                        <Grid item lg={7} sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                            <Stack justifyContent={'center'} ml={{ xs: 0, lg: 6}}px={{xs:1,lg:0}}>
                                <Typography width={{ xs: '70%', lg: '100%' }} sx={{ fontSize: { xs: 40, lg: 60 }, fontWeight: 'bold', color: '#00441D', mt: 2 }}>
                                    Download Ruchi App
                                </Typography>
                                <Typography sx={{ fontSize: {xs:20,lg:25}, color: '#130F26', width: { xs: '80%', lg: '70%' }, mt: 2 }}>
                                    And get all our goodness with a fantastic buying experience.
                                </Typography>
                                <Hidden smUp >
                                <img src='/images/android/Group1346.svg' style={{ height: 'auto', width: '100%',marginTop:14, }} />

                                </Hidden>


                                <Stack direction={{ xs: 'column', lg: 'row' }} mt={2}>
                                    <Box
                                        component="form"
                                        sx={{ p: '6px 4px', display: 'flex', alignItems: 'center', width: { xs: '100%', lg: 400 }, border: 1, borderRadius: 2, borderColor: '#00441D' }}
                                    >
                                        {/* <TextField
                                        sx={{ width: 400  ,}}
                                        placeholder="Email or Phone no."

                                        inputProps={{ 'aria-label': 'search google maps' }}
                                    /> */}
                                        <InputBase
                                            sx={{ ml: 1, flex: 1, }}
                                            placeholder="Email or Phone no."

                                            inputProps={{ 'aria-label': 'search google maps' }}
                                        />
                                    </Box>
                                    <Button variant='contained' sx={{
                                        bgcolor: '#F26147', textTransform: 'capitalize',
                                        borderRadius: 2, ml: { xs: 0, lg: 2 }, mt: { xs: 2, lg: 0 }, fontSize: 12, width: {xs:'120px',lg:'180px'},height:{xs:40,lg:50}
                                    }}>
                                        Share app link
                                    </Button>
                                </Stack>
                                <Typography sx={{ color: '#00441D', mt: { xs: 4, lg: 2 }, fontWeight: 'bold', fontSize: 15 }}>
                                    Download Ruchi app from
                                </Typography>
                                <Stack direction={'row'} spacing={3} mt={2} sx={{ justifyContent: { xs: 'center', lg: 'normal' } }}>
                                    <Stack direction={'row'} sx={{ bgcolor: 'black', alignItems: 'center', justifyContent: 'center', px: 1.5, borderRadius: 2 }}>
                                        <Avatar variant='square' src='/images/android/apple.svg' sx={{ height: '28px', width: 'auto' }} />
                                        <Stack ml={1}>
                                            <Typography sx={{ color: '#fff', fontSize: 10 }}>Download on the</Typography>
                                            <Typography sx={{ color: '#fff', fontSize: 18, }}>App Store</Typography>
                                        </Stack>
                                    </Stack>
                                    <Stack direction={'row'} sx={{ bgcolor: 'black', alignItems: 'center', justifyContent: 'center', px: 1.5, borderRadius: 2 }}>
                                        <Avatar variant='square' src='/images/android/apple.svg' sx={{ height: '28px', width: 'auto' }} />
                                        <Stack ml={1}>
                                            <Typography sx={{ color: '#fff', fontSize: 10 }}> GET IT ON </Typography>
                                            <Typography sx={{ color: '#fff', fontSize: 16, }}>Google Play</Typography>
                                        </Stack>
                                    </Stack>
                                </Stack>
                            </Stack>
                        </Grid>
                        <Hidden lgDown> 
                        <Grid item lg={5}>
                            <img src='/images/android/Group1346.svg' style={{ objectFit: 'cover', overflow: 'hidden', height: 'auto', width: '100%' }} />
                            {/* <Avatar variant='square' src='/images/android/Group1346.svg' sx={{height:'100vh',width:'auto'}} /> */}
                        </Grid>
                        </Hidden>
                       
                    </Grid>
                </Stack>
            </Stack>
        </Stack>

        </>
    )
}

export default RuchiApp