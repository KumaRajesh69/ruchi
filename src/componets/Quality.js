import { Avatar, Box, Grid, Stack, Typography } from '@mui/material'
import React from 'react'

const list = [
    {
        image: './images/ruchi/fssai.svg',
        about: "Started with the manufacturing of pure turmeric powder, and with the long journey of 46 years, the company has culminated into a full-grown tree spreading the aroma of its variety of Spices.."
    },
    {
        image: './images/ruchi/goi.svg',
        about: "Started with the manufacturing of pure turmeric powder, and with the long journey of 46 years, the company has culminated into a full-grown tree spreading the aroma of its variety of Spices.."
    },
    {
        image: './images/ruchi/haccp.svg',
        about: "Started with the manufacturing of pure turmeric powder, and with the long journey of 46 years, the company has culminated into a full-grown tree spreading the aroma of its variety of Spices.."
    },
]

function Quality() {
    return (
        <>
            <Stack sx={{ bgcolor: '#fff', justifyContent: 'center', alignItems: 'center', width: '100%', height: { xs: '100%', sm: '80vh', lg: '80vh' }, px: { xs: 0, lg: 2 } }}>
                <Grid container spacing={2}>
                    <Grid item xs={12} lg={12}>
                        <Stack justifyContent={{ xs: 'flex-start', lg: 'center' }} alignItems={{ xs: 'flex-start', lg: 'center' }} ml={{ xs: 2, lg: 0 }}>
                            <Typography sx={{
                                fontSize: { xs: 16, lg: 20 }, textAlign: 'center',
                                fontWeight: 'bold', color: '#130F26', mt: 2,
                            }}>
                                Excellence & Quality
                            </Typography>
                            <Typography sx={{
                                fontSize: { xs: 12, lg: 15 }, textAlign: 'center', fontWeight: 'bold',
                                color: '#2C2C2C',
                            }}>
                                Our Product standards speaks itself
                            </Typography>
                        </Stack>

                    </Grid>
                    <Grid item container xs={12} sx={{
                        width: '100%',
                        justifyContent: 'center', alignItems: 'center', my: { xs: 0, lg: 6 }, display: 'flex', flexDirection: 'row'
                    }}>
                        {/* <Stack direction='row' sx={{ width: '100%', justifyContent: 'center', alignItems: 'center', my: 6, px: {xs:10,sm:0,md:5,lg:10} }}> */}
                        {
                            list.map((each) => {
                                return (
                                    <Grid item xs={12} lg={4} sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', px: 2, width: '100%' }}>
                                        <Box height={'210px'} sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', }}>
                                            <img src={each.image} />
                                        </Box>
                                        <Typography sx={{ px: { xs: 8, sm: 2, md: 5, lg: 2 }, mt: 1, textAlign: 'center', color: '#07484A', }}>
                                            {each.about}
                                        </Typography>
                                    </Grid>
                                )
                            }
                            )
                        }
                        {/* </Stack> */}
                    </Grid>
                </Grid>

            </Stack>
        </>
    )
}

export default Quality