import { Typography, Button, Stack, Grid, Hidden, Avatar } from '@mui/material'
// import React from 'react'
import React, { useState } from 'react';
import { Organic } from './organic';



export const WholeSpices = () => {

    return (
        <>
           <Stack my={{xs:2,lg:5}}>
          
                <Grid container spacing={2} sx={{ display: 'flex', flexDirection: { xs: 'column-reverse', lg: 'row' } }}>
                    <Grid item lg={8} sx={{ zIndex: { xs: 1, lg: 1 },mt: { xs:-24, lg: 0 } }} >
                        <Organic />
                    </Grid>
                    <Hidden lgUp>
                        <Avatar variant='square' src='/images/Frame11.svg' sx={{
                            width: '100%', height: '15rem',
                            mt: {xs:-8,lg:-11}, zIndex: 0
                        }} />
                    </Hidden>
                    <Grid item lg={4} sx={{ zIndex: { xs: 1, lg: 1 }, }}>
                        <Stack px={{ xs: 1.2, lg: 0 }} >
                            <Stack direction={{ xs: 'row', lg: 'column' }}>
                                <Typography variant='h6' sx={{ fontSize: { xs: '60px', lg: '107px' }, fontWeight: '900', color: '#00441D', fontFamily: 'Playfair Display', mt: -2 }}>
                                    Whole
                                </Typography>
                                <Hidden lgUp>
                                    <Button variant='contained' sx={{
                                        borderRadius: 2, bgcolor: '#FFE944', color: '#000', mt: 2,
                                        textTransform: 'capitalize', height: 35, width: 80, fontSize: 13, mt: { xs: 2, lg: 10 }, ml: 3,
                                    }}>
                                        View all
                                    </Button>
                                </Hidden>
                            </Stack>
                            <Typography fontWeight="100" sx={{
                                fontSize: { xs: '70px', lg: '90px' },
                                color: {xs:'#FFE944',lg:'#000000'}, fontFamily: ' Recoleta', mt: { xs: -7.5, lg: -10 }, ml: { xs: 3, lg: 11 }
                            }}>
                                Spices
                            </Typography>
                         
                            <Hidden smDown>
                            <Hidden lgDown>
                                 <Typography sx={{ fontSize: { xs: 15, lg: 20 }, width: { xs: '100%', lg: '80%' }, color: '#fff', mt: { xs: 2, lg: -2 } }}>
                                Explore our some of the Bestseller packages.
                            </Typography>
                            </Hidden>
                           
                                <Button variant='contained' sx={{
                                    borderRadius: 2, bgcolor: '#FFE944', color: '#000', mt: 2,
                                    textTransform: 'capitalize', height: 35, width: 90,
                                }}>
                                    View all
                                </Button>
                            </Hidden>
                        </Stack>
                    </Grid>
                </Grid>
                <Hidden smDown>
                    <Avatar variant='square' src='/images/Frame11.svg' sx={{ width: '100%', height: 'auto', mt: -19 }} />
                </Hidden>
            {/* </Stack> */}
            </Stack>

        </>
    )
}

