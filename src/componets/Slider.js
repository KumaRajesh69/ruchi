import { Avatar, Box, Card, CardActions, Divider, Stack, Typography } from '@mui/material'
import React, { useState } from 'react'
import Slider from "react-slick";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import useMediaQuery from '@mui/material/useMediaQuery';
import theme from '../util/theme'
import { ruchiMockData } from '../util/mockdata';
import IconButton from '@mui/material/IconButton';
import FavoriteIcon from '@mui/icons-material/Favorite';





const SliderSection = () => {
    const [imageIndex, setImageIndex] = useState(1);

    const NextArrow = ({ onClick }) => {
        return (
            <Box sx={{
                "backgroundColor": "#fff",
                "position": "absolute",
                "cursor": "pointer",
                "zIndex": "10", "right": "4%",
                "top": "50%", display: 'flex', alignItems: 'center', justifyContent: 'center', borderRadius: 2, p: 0.5
            }} onClick={onClick}>
                <ArrowForwardIcon />
            </Box>
        );
    };

    // @ts-ignore
    const PrevArrow = ({ onClick }) => {
        return (
            <Box sx={{
                "backgroundColor": "#fff",
                "position": "absolute",
                "cursor": "pointer",
                "zIndex": "10", "left": "4%",
                "top": "50%", display: 'flex', alignItems: 'center', justifyContent: 'center', borderRadius: 2, p: 0.5
            }} onClick={onClick}>
                <ArrowBackIcon />
            </Box>
        );
    };


    const settings = {
        infinite: true,
        lazyLoad: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 4,
        centerMode: true,
        centerPadding: 0,
        initialSlide: 2,
        nextArrow: <NextArrow onClick={undefined} />,
        prevArrow: <PrevArrow onClick={undefined} />,
        beforeChange: (current, next) => setImageIndex(next),
        // dots: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    infinite: false,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1
                }
            }
        ]
    };

    // const cardContents = [
    //     {
    //         img:'/images/ruchi/ruchi-img.svg',
    //         name:'Ruchi CHILLI POWDER 5...',
    //         weight:'500gm',
    //         ratings:4.9,
    //         reviews:300,
    //         price:120,
    //         actualPrice:500,
    //         discount:'40% off',
    //     },
    //     {
    //         name: "PC/ CONSOLE/ ESPORTS",
    //         title: "VR RACING/DRIVING SIMULATOR",
    //         img: '/images/ruchi/ruchi-img.svg'

    //     },
    //     {
    //         name: "PC/ CONSOLE/ ESPORTS",
    //         title: "VR RACING/DRIVING SIMULATOR",
    //         img: '/images/ruchi/ruchi-img.svg'

    //     },
    //     {
    //         name: "PC/ CONSOLE/ ESPORTS",
    //         title: "VR RACING/DRIVING SIMULATOR",
    //         img: '/images/ruchi/ruchi-img.svg'

    //     },
    //     {
    //         name: "PC/ CONSOLE/ ESPORTS",
    //         title: "VR RACING/DRIVING SIMULATOR",
    //         img: '/images/ruchi/ruchi-img.svg'

    //     },
    //     {
    //         name: "PC/ CONSOLE/ ESPORTS",
    //         title: "VR RACING/DRIVING SIMULATOR",
    //         img: '/images/ruchi/ruchi-img.svg'

    //     },
    // ]
    const [isLiked, setIsLiked] = useState(false);

    const handleLikeClick = () => {
        setIsLiked(!isLiked);
    };
    const tablet = useMediaQuery(theme.breakpoints.only('sm'));
    const mobile = useMediaQuery(theme.breakpoints.only('xs'));
    const desktop = useMediaQuery(theme.breakpoints.up('md'));

    return (
        <>
            <Box
            //   mt={{ md: 3, sm: 2, xs: 0 }} sx={{ p: { lg: 2, md: 2, sm: 2, xs: 0 } }}
              >
                <Slider  >
                    {ruchiMockData.map((each) => (
                        <Card
                            // key={idx + 1}
                            // sx={idx === imageIndex ? {
                            //     "transform": mobile ? "scale(0.7)" : tablet ? "scale(0.9)" : "scale(0.8)",
                            //     "transition": "transform 300ms",
                            //     borderRadius: 2,
                            //     height: { xs: 200, sm: 230, md: 300, lg: 250 },
                            //     width: "auto",
                            //     backgroundImage: `url('${e.img}')`,
                            //     backgroundSize: '100vh auto',
                            //     objectFit: 'cover',
                            //     backgroundPosition: 'center',
                            //     backgroundRepeat: "no-repeat",
                            //     // p: 2
                            // } :
                            //     {
                            //         "transform": mobile ? "scale(0.7)" : tablet ? "scale(0.6)" : "scale(0.6)",
                            //         "transition": "transform 300ms",
                            //         borderRadius: 2,
                            //         height: { xs: 200, sm: 230, md: 300, lg: 250 },
                            //         width: "auto",
                            //         backgroundImage: `url('${e.img}')`,
                            //         backgroundSize: '100vh auto',
                            //         objectFit: 'cover',
                            //         backgroundPosition: 'center',
                            //         backgroundRepeat: "no-repeat",
                            //         p: 2
                            //     }
                            // }
                            sx={{ maxWidth: 250, borderRadius: 3, }}
                        >
                            {/* <CardActions
                                sx={{
                                    height: '100%',
                                    flexDirection: 'row',
                                    width: '100%',
                                    alignItems: 'end',
                                }}
                            > */}
                            {/* <Stack>
                                    <Typography
                                        variant='caption'
                                        color={'#FFE070'}
                                        sx={{ textAlign: 'start', }}>
                                        {e?.name}
                                    </Typography>

                                    <Typography variant='subtitle1' color={'#FFFFFF'}>
                                        {e?.title}
                                    </Typography>
                                </Stack> */}
                            <Stack sx={{ px: 2 }}>
                                <Stack direction={'row'} justifyContent={'flex-end'} width={'100%'} mt={1}>
                                    <IconButton aria-label="like"
                                        onClick={handleLikeClick}
                                        sx={{ color: isLiked ? 'red' : 'inherit', }}
                                    >
                                        <FavoriteIcon />
                                    </IconButton>
                                </Stack>
                                <Stack justifyContent={'center'} alignItems={'center'} >
                                    <Avatar variant='square' src={each.img}
                                        sx={{ width: '108px', height: '100px', }}
                                    />
                                </Stack>

                                <Typography sx={{
                                    fontWeight: 'bold', whiteSpace: 'nowrap', width: '90%', overflow: 'hidden', textOverflow: 'ellipsis', mt: 2
                                }}>
                                    {each.name}
                                </Typography>
                                <Typography sx={{ color: '#6F6F6F', fontSize: 14, mt: 1 }}>
                                    {each.weight}
                                </Typography>
                                <Stack direction={'row'} alignItems={'center'} mt={1}>
                                    <Box sx={{
                                        display: 'flex', flexDirection: 'row', bgcolor: '#1C9050', justifyContent: 'space-between',
                                        alignItems: 'center', width: '70px', height: '30px', px: 1.2, py: 0.9, borderRadius: 2
                                    }}>
                                        <Avatar variant='square' src='/images/Star.svg' sx={{ height: '14px', width: '14px' }} />
                                        <Divider orientation="vertical" sx={{ bgcolor: '#fff', fontWeight: 'bold' }} />
                                        <Typography sx={{ fontSize: 14, color: '#fff' }}>
                                            {each.ratings}
                                        </Typography>
                                    </Box>
                                    <Typography sx={{ ml: 1, color: '#6E6E6E', fontSize: 12 }}>
                                        {each.reviews}
                                    </Typography>
                                </Stack>

                                <Stack direction={'row'} alignItems={'center'} mt={1}>
                                    <Typography sx={{ fontWeight: 'bold', fontSize: 20, color: '#000000' }}>₹{each.price}</Typography>
                                    <Typography sx={{ textDecorationLine: 'line-through', color: '#6F6F6F', fontSize: 14, ml: 1 }}>₹{each.actualPrice}</Typography>
                                    <Typography sx={{ color: '#1C9050', fontSize: 16, ml: 1 }}>{each.discount}</Typography>
                                </Stack>

                            </Stack>
                            {/* </CardActions> */}
                        </Card>
                    ))}
                </Slider>
            </Box >
        </>
    )
}

export default SliderSection