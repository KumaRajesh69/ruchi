import { Avatar, Box, Grid, Stack, Typography, useMediaQuery } from '@mui/material'
import theme from '../util/theme'
import React from 'react'

function YearOfRuch() {

    const mobile = useMediaQuery(theme.breakpoints.only('xs'));
    const laptop = useMediaQuery(theme.breakpoints.up('md'));

    return (
        <>
        <Stack mt={3}>

            <Stack sx={{ position: 'relative', zIndex: 0, }}>
             {
                mobile &&
                <Avatar variant='square' src='/images/yearOfRuchi/YrMobilBg.svg' sx={{width:'auto',height:'100vh'}} />
             }
             {
                laptop &&
                <Avatar variant='square' src='/images/yearOfRuchi/yrLapBg.svg' sx={{width:'100%',height:'auto'}} />

             }
                <Grid container spacing={2} sx={{ position: 'absolute', zIndex: 1,mt:{xs:0,lg:2} }}>
                    <Grid item xs={12} lg={4.5} sx={{ display: 'flex', justifyContent: {xs:'center',lg:'flex-end'}, alignItems: 'center' }}>
                        {
                            mobile &&
                            <img src='/images/yearOfRuchi/yearOfRuchi2.svg' />
                        }
                        {
                            laptop &&
                            <img src='/images/yearOfRuchi/yearOfRuchi1.svg' />
                        }

                    </Grid>
                    <Grid item lg={.5} />
                    <Grid item xs={12} lg={7} sx={{ display: 'flex', flexDirection: 'column', alignItems: { xs: 'center', lg: 'flex-start', justifyContent: 'center' } }}>
                        <Typography sx={{ fontSize: 22, fontWeight: 'bold', color: '#D20C0C', width: '80%', textAlign: { xs: 'center', lg: 'left' } }}>
                            46 Years of Quality & Excellence
                        </Typography>
                        <Typography sx={{ fontSize:  {xs:12,lg:15}, color: '#036425', width: {xs:'80%',lg:'80%'}, mt: 2, textAlign: { xs: 'center', lg: 'left' } }}>
                            It was the year 1976, with a mere investment and determination, a small enterprise by the
                            name of Om Oil & Flour Mills was started in a rented premises in Industrial Estate, Cuttack.
                        </Typography>
                        <Typography sx={{ fontSize: {xs:12,lg:15}, color: '#036425', width: '80%', mt: 2, textAlign: { xs: 'center', lg: 'left' } }}>
                            Started with the manufacturing of pure turmeric powder, and with the long journey of 46 years,
                            the company has culminated into a full-grown tree spreading the aroma of it's variety of Spices
                            to different parts of the globe. Apart from spices, today RUCHI is the leading manufacturer and
                            exporter of Pasta, Noodles, Ready-to-Eat and other food products.
                        </Typography>
                    </Grid>
                </Grid>
            </Stack>
            </Stack>

        </>
    )
}

export default YearOfRuch