import { Avatar, Box, Button, Hidden, Stack, Typography, useMediaQuery } from '@mui/material'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import theme from '../util/theme'

import React from 'react'

export const BeReady = () => {

    const mobile = useMediaQuery(theme.breakpoints.only('xs'));
    const laptop = useMediaQuery(theme.breakpoints.up('md'));

    return (
        <>
            <Stack sx={{ position: 'relative', zIndex: 0 }}>
                 {
                        mobile &&
                        <Avatar variant='square' src='/images/beready/bg.svg' sx={{ height: '100vh', width: 'auto' }} />
                    }
                     {
                        laptop &&
                        <Avatar variant='square' src='/images/beready/bereadybg.svg' sx={{ height: '100vh', width: 'auto' }} />
                    }
                <Stack sx={{ position: 'absolute', zIndex: 1, width: { xs: '100%', lg: '50%' } }}>
                    <Stack ml={{ xs: 2, lg: 10 }} mt={{xs:0,lg:5}}>
                        <Typography sx={{
                            fontSize: { xs: 20, lg: 40 }, mt: 4, fontWeight: 'bold', color: '#036425', width: { xs: '60%', lg: '60%' },
                            fontFamily: 'Playfair Display',
                        }}>
                            Be ready to make your day spicy.
                        </Typography>
                        <Typography sx={{ color: '#036425', mt: 4, fontSize: { xs: 13, lg: 14 }, width: { xs: '79%', lg: '80%' } }}>
                            Born in Odisha, Pride of India, Reaching the Globe.
                            We are the leading manufacturer and exporter of Quality Spices, Pasta, Noodles, Ready-to-Eat Frozen
                            Food Products and various Bakery Products.
                        </Typography>
                        <Hidden smUp>
                        <Box sx={{display:'flex',justifyContent:'center',alignItems:'center',mt:2}}>
                            <img src='/images/beready/ruchiSpice.svg' />
                        </Box>
                        </Hidden>
                        <Button variant='contained' sx={{
                            bgcolor: '#F26147', mt: 5, textTransform: 'capitalize', width: '30%', borderRadius: 3, height: '51px', width: '230px'
                            , fontSize: 15,ml:{xs:3.5,lg:0}
                        }}>
                            Explore Our Range
                            < ArrowForwardIcon sx={{ ml: 1 }} />
                        </Button>
                    </Stack>
                </Stack>

            </Stack>
        </>
    )
}
