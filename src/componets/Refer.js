import { Avatar, Button, Card, Hidden, Stack, Typography } from '@mui/material'
import React from 'react'

export const Refer = () => {
    return (
        <>
            <Stack direction={{ xs: 'column', lg: 'row', }} spacing={2} px={{ lg: 10, }} py={{ sx: 2, lg: 4 }}>
                <Stack mt={{xs:2,lg:0}}>
                    <Typography sx={{ fontWeight: 'bold', ml: { lg: 0, xs: 2 } }}>
                        Refer & earn
                    </Typography>
                    <Typography sx={{ fontSize: 12, ml: { lg: 0, xs: 2 } }}>
                        Refer to your friends & earn Discounts
                    </Typography>
                    <Card sx={{ borderRadius: 5 ,mt:2}}>
                        <Avatar variant='square' src='/images/refer/woman.svg' sx={{ height: '300px', width: 'auto', }} />
                    </Card>
                </Stack>
                <Stack px={{xs:2,lg:0}}>
                    <Stack direction={{ xs: 'row', lg: 'column' }} justifyContent={{ xs: 'space-between', lg: 'none' }}>
                        <Stack >
                            <Typography sx={{ fontWeight: 'bold' }}>
                                Our Recepies
                            </Typography>
                            <Typography sx={{ fontSize: {xs:10,lg:12}, }}>
                                Refer to your friends & earn Discounts
                            </Typography>
                        </Stack>
                        <Hidden lgUp>
                            <Button variant='contained' sx={{ borderRadius: 3, width:'35%', bgcolor: '#FFC726', }}>
                                View all
                            </Button>
                        </Hidden>

                    </Stack>
                    <Card sx={{ borderRadius: 5 ,mt:2 }}>
                        <Avatar variant='square' src='/images/refer/food1.svg' sx={{ height: '305px', width: 'auto' }} />
                    </Card>
                </Stack>
                <Stack spacing={1} px={{xs:2,lg:0}}>
                    <Stack flexDirection={'row-reverse'} width={'100%'} mt={2}>
                        <Hidden smDown>
                            <Button sx={{ width: '20%', bgcolor: '#FFC726', borderRadius: 3, fontSize: 12, textTransform: 'capitalize' }}>
                                View all
                            </Button>
                        </Hidden>
                    </Stack>

                    <Stack direction={'row'} spacing={2} width={'100%'} >
                      
                        <Avatar variant='square' src='/images/refer/food2.svg' sx={{ height: '140px', width: 'auto' }} />
                        <Stack>
                            <Typography sx={{ fontSize: 12, fontWeight: 'bold', width: '50%' }}>How to prepare a tasty & mouthpleasing Chicken Curry</Typography>
                            <Typography>by Bijay Ranjan</Typography>
                            <Typography>#nonveg #chicken</Typography>
                        </Stack>
                 
                    </Stack>
                    <Stack direction={'row'} spacing={2} width={'100%'} >
                        <Avatar variant='square' src='/images/refer/food2.svg' sx={{ height: '140px', width: 'auto' }} />
                        <Stack>
                            <Typography sx={{ fontSize: 12, fontWeight: 'bold', width: '50%' }}>How to prepare a tasty & mouthpleasing Chicken Curry</Typography>
                            <Typography>by Bijay Ranjan</Typography>
                            <Typography>#nonveg #chicken</Typography>
                        </Stack>
                    </Stack>
                </Stack>
            </Stack>
        </>
    )
}
