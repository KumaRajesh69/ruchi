import { Box, Stack, Typography } from '@mui/material'
import React from 'react'

function Brand() {
    return (
        <>
            <Stack sx={{ p: 2, width: '100%' ,bgcolor:'#EFEFEF', }}>
                <Typography sx={{ ml: 7.4, fontWeight: 'bold',fontSize:18 }}>
                    Brand Directory
                </Typography>
                <Stack direction={'row'} spacing={2} mt={2}>
                    <Box sx={{ width: '15%', textAlign: 'right' }}>
                        <Typography sx={{ fontSize: 12, fontWeight: 'bold' }}>
                            Most Searched on Ruchi:
                        </Typography>
                    </Box>
                    <Typography width={'85%'} sx={{ fontSize: 12,color:'#707070' }}>
                        Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder | Vernicilli | Tezlal Chilli powder
                        | Kashmiri Chilli Powder | Noodles |  Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder
                        | Vernicilli | Tezlal Chilli powder | Kashmiri Chilli Powder | Noodles |
                    </Typography>
                </Stack>
                <Stack direction={'row'} spacing={2}>
                    <Box sx={{ width: '15%', textAlign: 'right' }}>
                        <Typography sx={{ fontSize: 12, fontWeight: 'bold' }}>
                        Basic Spices:
                        </Typography>
                    </Box>
                    <Typography width={'85%'} sx={{ fontSize: 12,color:'#707070' }}>
                        Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder | Vernicilli | Tezlal Chilli powder
                        | Kashmiri Chilli Powder | Noodles |  Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder
                        | Vernicilli | Tezlal Chilli powder | Kashmiri Chilli Powder | Noodles |
                    </Typography>
                </Stack>
                <Stack direction={'row'} spacing={2}>
                    <Box sx={{ width: '15%', textAlign: 'right' }}>
                        <Typography sx={{ fontSize: 12, fontWeight: 'bold' }}>
                            Most Searched on Ruchi:
                        </Typography>
                    </Box>
                    <Typography width={'85%'} sx={{ fontSize: 12,color:'#707070' }}>
                        Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder | Vernicilli | Tezlal Chilli powder
                        | Kashmiri Chilli Powder | Noodles |  Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder
                        | Vernicilli | Tezlal Chilli powder | Kashmiri Chilli Powder | Noodles |
                    </Typography>
                </Stack>
                <Stack direction={'row'} spacing={2}>
                    <Box sx={{ width: '15%', textAlign: 'right' }}>
                        <Typography sx={{ fontSize: 12, fontWeight: 'bold' }}>
                        Basic Spices:
                        </Typography>
                    </Box>
                    <Typography width={'85%'} sx={{ fontSize: 12,color:'#707070' }}>
                        Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder | Vernicilli | Tezlal Chilli powder
                        | Kashmiri Chilli Powder | Noodles |  Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder
                        | Vernicilli | Tezlal Chilli powder | Kashmiri Chilli Powder | Noodles |
                    </Typography>
                </Stack>
                <Stack direction={'row'} spacing={2}>
                    <Box sx={{ width: '15%', textAlign: 'right' }}>
                        <Typography sx={{ fontSize: 12, fontWeight: 'bold' }}>
                            Most Searched on Ruchi:
                        </Typography>
                    </Box>
                    <Typography width={'85%'} sx={{ fontSize: 12 ,color:'#707070'}}>
                        Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder | Vernicilli | Tezlal Chilli powder
                        | Kashmiri Chilli Powder | Noodles |  Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder
                        | Vernicilli | Tezlal Chilli powder | Kashmiri Chilli Powder | Noodles |
                    </Typography>
                </Stack>
                <Stack direction={'row'} spacing={2}>
                    <Box sx={{ width: '15%', textAlign: 'right' }}>
                        <Typography sx={{ fontSize: 12, fontWeight: 'bold' }}>
                        Basic Spices:
                        </Typography>
                    </Box>
                    <Typography width={'85%'} sx={{ fontSize: 12,color:'#707070' }}>
                        Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder | Vernicilli | Tezlal Chilli powder
                        | Kashmiri Chilli Powder | Noodles |  Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder
                        | Vernicilli | Tezlal Chilli powder | Kashmiri Chilli Powder | Noodles |
                    </Typography>
                </Stack>
                <Stack direction={'row'} spacing={2}>
                    <Box sx={{ width: '15%', textAlign: 'right' }}>
                        <Typography sx={{ fontSize: 12, fontWeight: 'bold' }}>
                            Most Searched on Ruchi:
                        </Typography>
                    </Box>
                    <Typography width={'85%'} sx={{ fontSize: 12 ,color:'#707070'}}>
                        Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder | Vernicilli | Tezlal Chilli powder
                        | Kashmiri Chilli Powder | Noodles |  Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder
                        | Vernicilli | Tezlal Chilli powder | Kashmiri Chilli Powder | Noodles |
                    </Typography>
                </Stack>
                <Stack direction={'row'} spacing={2}>
                    <Box sx={{ width: '15%', textAlign: 'right' }}>
                        <Typography sx={{ fontSize: 12, fontWeight: 'bold' }}>
                        Basic Spices:
                        </Typography>
                    </Box>
                    <Typography width={'85%'} sx={{ fontSize: 12,color:'#707070' }}>
                        Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder | Vernicilli | Tezlal Chilli powder
                        | Kashmiri Chilli Powder | Noodles |  Turmeric Powder | Red Chilli Powder | Cumin Powder Jeera | Coriander Powder
                        | Vernicilli | Tezlal Chilli powder | Kashmiri Chilli Powder | Noodles |
                    </Typography>
                </Stack>
            </Stack>

        </>
    )
}

export default Brand