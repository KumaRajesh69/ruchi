import { Grid, Stack, Typography, Button, Avatar, Hidden } from '@mui/material'
// import React from 'react'
import React, { useState } from 'react';
import { ArrowBack, ArrowForward } from '@mui/icons-material';
import { SwiperPage } from './SwiperPage';
import { Organic } from './organic';



export const BasicSpices = () => {

    return (
        <>
            <Stack my={2}>
                <Stack sx={{ height: 'auto',}}>
                    <Grid container spacing={2} sx={{ zIndex: 1 }}>
                        <Grid item lg={4} sx={{ zIndex: { xs: 1, lg: 'none' } }}>
                            <Stack ml={{ xs: 0, lg: 4 }} mt={{xs:0,lg:6}}>
                                <Stack direction={'row'} px={{xs:1,lg:0}}>
                                    <Typography variant='h6' sx={{ fontSize: { xs: '90px', lg: '107px' }, fontWeight: '900', color: '#0A6510', fontFamily: 'Playfair Display', mt: {xs:-3,lg:1} }}>
                                        Basic
                                    </Typography>
                                    <Button variant='contained' sx={{
                                        borderRadius: 2, bgcolor: '#FFE944', color: '#000', mt: { xs: 5, lg: 11 }, ml: {xs:1,lg:2},
                                        textTransform: 'capitalize', height: 35, width: {xs:80,lg:79}, fontSize: 13
                                    }}>
                                        View all
                                    </Button>
                                </Stack>

                                <Typography fontWeight="100" fontSize={{ xs: '75px', lg: '107px' }}
                                    sx={{ color: '#FFE944', fontFamily: 'Playfair Display', mt: { xs: -10.5, lg: -14 }, ml: { xs: 3.7, lg: 3.5 } }}>
                                  spices
                                </Typography>
                                <Hidden lgDown>
                                <Typography sx={{ fontSize: 20, width: '80%', color: '#fff',  }}>
                                    Explore our some of the Bestseller spices.
                                </Typography>
                                </Hidden>
                             
                            </Stack>
                        </Grid>
                        <Hidden lgUp>
                            <Avatar variant='square' src='/images/Frame11.svg' sx={{
                                width: '100%', height: '15rem',
                                mt: {xs:-8,lg:-13}, zIndex: 0
                            }} />
                        </Hidden>
                        <Grid item lg={8} sx={{ zIndex: { xs: 1, lg: 0 }, mt: { xs: -24, lg: 0 } }}>
                            <Organic />
                        </Grid>
                    </Grid>
                    <Hidden smDown>
                        <Avatar variant='square' src='/images/Frame11.svg' sx={{ width: '100%', height: '12rem', mt: -19 }} />
                    </Hidden>
                </Stack>
            </Stack>
        </>
    )
}


