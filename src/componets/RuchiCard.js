import { Avatar, Card, Stack, Box, Divider } from '@mui/material'
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import React, { useState } from 'react'

function RuchiCard() {
    // const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
    const [isLiked, setIsLiked] = useState(false);

    const handleLikeClick = () => {
        setIsLiked(!isLiked);
    };

    return (
        <>
            <Card sx={{ maxWidth: 250, borderRadius: 3, m:2}}>
                <Stack  sx={{ px:2 }}>
                    <Stack direction={'row'} justifyContent={'flex-end'} width={'100%'} mt={1}>
                        <IconButton aria-label="like"
                            onClick={handleLikeClick}
                            sx={{ color: isLiked ? 'red' : 'inherit', }}
                        >
                            <FavoriteIcon />
                        </IconButton>
                    </Stack>
                    <Stack justifyContent={'center'} alignItems={'center'} >
                        <Avatar variant='square' src='/images/ruchi/ruchi-img.svg'
                            sx={{ width: '108px', height: '100px', }}
                        />
                    </Stack>

                    <Typography sx={{
                        fontWeight: 'bold', whiteSpace: 'nowrap', width: '90%', overflow: 'hidden', textOverflow: 'ellipsis',mt:2
                    }}>
                        Ruchi CHILLI POWDER 5 gm in ruchi
                    </Typography>
                    <Typography sx={{ color: '#6F6F6F', fontSize: 14,mt:.5 }}>
                        500 gm
                    </Typography>
                    <Stack direction={'row'} alignItems={'center'} mt={1}>
                        <Box sx={{display:'flex',flexDirection:'row', bgcolor: '#1C9050' ,justifyContent:'space-between',
                        alignItems:'center',width:'70px',height:'30px' ,px:1.2,py:0.9,borderRadius:2 }}>
                            <Avatar variant='square' src='/images/Star.svg' sx={{height:'14px',width:'14px'}} />
                            <Divider orientation="vertical"  sx={{bgcolor:'#fff',fontWeight:'bold'}} />
                            <Typography sx={{fontSize:14,color:'#fff'}}>
                                4.9
                            </Typography>
                        </Box>
                        <Typography sx={{ml:1,color:'#6E6E6E',fontSize:12}}>
                            300 Reviews
                        </Typography>
                    </Stack>
                    
                    <Stack direction={'row'} alignItems={'center'} mt={1}>
                        <Typography sx={{fontWeight:'bold',fontSize:20,color:'#000000'}}>₹129</Typography>
                        <Typography sx={{ textDecorationLine: 'line-through',color: '#6F6F6F', fontSize: 14,ml:1}}>₹500</Typography>
                        <Typography sx={{color: '#1C9050', fontSize: 16,ml:1}}>40% Off</Typography>
                    </Stack>
                    
                </Stack>
                <Stack width={'100%'} mt={1} >
                        <Button variant='contained' sx={{bgcolor:'#000'}}>
                            <ShoppingCartIcon />
                            Add to cart
                        </Button>
                    </Stack>
            </Card>
           
        </>
    )
}

export default RuchiCard