import { Avatar, Fab, Stack, Box, Divider, Paper } from '@mui/material'
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import React, { useRef, useState } from 'react'


function DisplayOffer() {

  const [isLiked, setIsLiked] = useState(false);

  const scrollRef = useRef();

  const handleLikeClick = () => {
    setIsLiked(!isLiked);
  };

  const scrollRight = () => {
    console.log('Scrol ref ---> ', scrollRef);
    scrollRef.current.scrollLeft += 200;
  }
  const scrollLeft = () => {
    console.log('Scrol ref ---> ', scrollRef);
    scrollRef.current.scrollLeft -= 200;
  }

  const list = [
    {
      img: '/images/desktop/Frame31.svg'
    },
    {
      img: '/images/desktop/Frame31.svg'
    },
    {
      img: '/images/desktop/Frame31.svg'
    },
  ]

  return (
    <>
      <Stack direction={'row'} sx={{ alignItems: 'center' }}>
        <Stack sx={{width:'100%',height:'360px',
          backgroundImage: 'url("/images/desktop/Rectangle910.svg")',
          backgroundSize: 'cover', // Optional: Adjust the image size according to your needs
          backgroundRepeat: 'no-repeat', // Optional: Control how the image repeats
        }}>


          <Stack direction={'row'} sx={{
            width: '100%',
            overflowX: 'auto',
            scrollBehavior: 'smooth', 
            '::-webkit-scrollbar': {
              display: 'none'
            },
            '-ms-overflow-style': 'none',
            'scrollbar-width': 'none',mt:4
          }}
            ref={scrollRef}
          >
            <Stack direction={'row'} spacing={3}>
              {
                list.map((each) => (

                  <Avatar variant='square' src={each.img} sx={{ height: 'auto', width: '1215px' }} />

                ))
              }
            </Stack>
          </Stack>
        </Stack>
      </Stack>
    </>

  )
}

export default DisplayOffer