import { Stack, Typography } from '@mui/material'
import React from 'react'

function AboutRuchi() {
    return (
        <>
        <Stack sx={{px:10,py:2}}>
        <Stack >
            <Typography  sx={{fontWeight:'bold',fontSize:15}}>
                Ruchi: Made from Odisha Pride of India
            </Typography>
            <Typography sx={{color:'#707070',fontSize:15}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet aliquet facilisis porta et consequat tincidunt.
                Vel congue mattis ultrices diam laoreet vitae pulvinar erat. Netus nulla enim sit purus ultricies tellus, elementum.
                Nulla nunc, pharetra habitasse pellentesque velit dolor. Scelerisque metus sed imperdiet vestibulum, mi nullam sed.
                Scelerisque volutpat euismod ultricies facilisi feugiat. Tempus ac pellentesque nibh nam proin ut eget.
                Proin volutpat urna, sed tellus. Nullam proin risus diam suspendisse laoreet at. Non eleifend ac fringilla iaculis massa.
                Mauris est facilisis non mauris. Euismod erat eu, congue eget vel. Porta nec tortor platea leo.
                At magna nibh euismod tellus pellentesque porttitor dictum egestas.
                Dolor vitae enim ultrices nisl cursus cursus eget consectetur. Imperdiet sit arcu ornare odio diam ornare sit.
                Habitant bibendum risus amet, risus vestibulum rhoncus. Enim, aenean malesuada enim integer fermentum aliquet.
                Adipiscing risus semper mi at egestas. Nunc, quis risus ornare volutpat. Felis, risus hac pulvinar at at.
            </Typography>

            <Typography variant='body1' sx={{color:'#707070',mt:2,fontSize:15}}>
                Libero ut in morbi etiam leo egestas justo. Molestie semper viverra mus neque.
                Id facilisi nulla mi pulvinar sed faucibus. Fermentum vestibulum diam non libero, metus, at pellentesque.
                Varius nulla commodo arcu blandit et in. Ut feugiat in est a in. Amet, arcu, pellentesque luctus libero.
                Dolor dictum duis sit eget vestibulum sit urna hendrerit. Pulvinar suscipit aenean elit, amet consequat,
                vitae tortor vestibulum amet. Leo pharetra, arcu fringilla quam diam diam, feugiat. Convallis mi viverra dis magna
                faucibus. Quam tempor adipiscing vitae sed in. Tellus diam suscipit et posuere turpis.
                At dolor aenean sed vivamus pharetra ut. Condimentum malesuada consequat vel lectus. Vel cursus cras adipiscing
                blandit molestie massa in eu. Arcu adipiscing velit vestibulum vitae quam ut nunc, turpis posuere.
                Sollicitudin tristique eu accumsan ornare vel tempus et, egestas. Facilisi ultricies facilisis quis dui enim eros,
                tortor molestie sit. Quam a donec amet quisque. Lectus morbi ornare morbi viverra risus parturient sodales ut.
            </Typography>
            </Stack>

            <Typography  sx={{fontWeight:'bold',fontSize:15,mt:3}}>
                Ruchi: Made from Odisha Pride of India
            </Typography>
            <Typography sx={{color:'#707070',fontSize:15}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet aliquet facilisis porta et consequat tincidunt.
                Vel congue mattis ultrices diam laoreet vitae pulvinar erat. Netus nulla enim sit purus ultricies tellus, elementum.
                Nulla nunc, pharetra habitasse pellentesque velit dolor. Scelerisque metus sed imperdiet vestibulum, mi nullam sed.
                Scelerisque volutpat euismod ultricies facilisi feugiat. Tempus ac pellentesque nibh nam proin ut eget.
                Proin volutpat urna, sed tellus. Nullam proin risus diam suspendisse laoreet at. Non eleifend ac fringilla iaculis massa.
                Mauris est facilisis non mauris. Euismod erat eu, congue eget vel. Porta nec tortor platea leo.
                At magna nibh euismod tellus pellentesque porttitor dictum egestas.
                Dolor vitae enim ultrices nisl cursus cursus eget consectetur. Imperdiet sit arcu ornare odio diam ornare sit.
                Habitant bibendum risus amet, risus vestibulum rhoncus. Enim, aenean malesuada enim integer fermentum aliquet.
                Adipiscing risus semper mi at egestas. Nunc, quis risus ornare volutpat. Felis, risus hac pulvinar at at.
            </Typography>

            <Typography  sx={{fontWeight:'bold',fontSize:15,mt:3}}>
                Ruchi: Made from Odisha Pride of India
            </Typography>
            <Typography sx={{color:'#707070',fontSize:15}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet aliquet facilisis porta et consequat tincidunt.
                Vel congue mattis ultrices diam laoreet vitae pulvinar erat. Netus nulla enim sit purus ultricies tellus, elementum.
                Nulla nunc, pharetra habitasse pellentesque velit dolor. Scelerisque metus sed imperdiet vestibulum, mi nullam sed.
                Scelerisque volutpat euismod ultricies facilisi feugiat. Tempus ac pellentesque nibh nam proin ut eget.
                Proin volutpat urna, sed tellus. Nullam proin risus diam suspendisse laoreet at. Non eleifend ac fringilla iaculis massa.
                Mauris est facilisis non mauris. Euismod erat eu, congue eget vel. Porta nec tortor platea leo.
                At magna nibh euismod tellus pellentesque porttitor dictum egestas.
                Dolor vitae enim ultrices nisl cursus cursus eget consectetur. Imperdiet sit arcu ornare odio diam ornare sit.
                Habitant bibendum risus amet, risus vestibulum rhoncus. Enim, aenean malesuada enim integer fermentum aliquet.
                Adipiscing risus semper mi at egestas. Nunc, quis risus ornare volutpat. Felis, risus hac pulvinar at at.
            </Typography>

        </Stack>

        </>

    )
}

export default AboutRuchi