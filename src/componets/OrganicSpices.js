import { Grid, Stack, Typography, Button, Avatar, Hidden } from '@mui/material'
// import React from 'react'
import React, { useState } from 'react';
import { ArrowBack, ArrowForward } from '@mui/icons-material';
import { SwiperPage } from './SwiperPage';
import { Organic } from './organic';



export const OrganicSpices = () => {

    return (
        <>
            <Stack mt={2}>
                <Stack sx={{ height: 'auto' }}>
                    <Grid container spacing={2} sx={{ zIndex: 1 }}>

                        <Grid item lg={4} sx={{ zIndex: { xs: 1, lg: 'none' } }}>
                            <Stack ml={{ xs: 0, lg: 4 }}>
                                <Stack direction={'row'}>
                                    <Typography variant='h6' sx={{ fontSize: { xs: '60px', lg: '80px' }, fontWeight: '900', color: '#0A6510', fontFamily: 'Playfair Display', mt: {xs:-1,lg:3} }}>
                                        Organic
                                    </Typography>
                                    <Button variant='contained' sx={{
                                        borderRadius: 2, bgcolor: '#FFE944', color: '#000', mt: { xs:4.9, lg: 10 }, ml: 3,
                                        textTransform: 'capitalize', height: 35, width: 80, fontSize: 13
                                    }}>
                                        View all
                                    </Button>
                                </Stack>

                                <Typography fontWeight="100" fontSize={{ xs: '75px', lg: '107px' }}
                                    sx={{ color: '#FFE944', fontFamily: 'Playfair Display', mt: { xs: -7, lg: -10 }, ml: { xs: 6, lg: 6 } }}>
                                  spices
                                </Typography>
                                <Hidden lgDown>
                                <Typography sx={{ fontSize: 20, width: '80%', color: '#fff', mt: -2 }}>
                                    Explore our some of the Bestseller spices.
                                </Typography>     
                                </Hidden>
                               
                            </Stack>
                        </Grid>
                        <Hidden lgUp>
                            <Avatar variant='square' src='/images/Frame11.svg' sx={{
                                width: '100%', height: '15rem',
                                mt: -7, zIndex: 0
                            }} />
                        </Hidden>
                        <Grid item lg={8} sx={{ zIndex: { xs: 1, lg: 0 }, mt: { xs: -25, lg: 0 } }}>
                            <Organic />
                        </Grid>
                    </Grid>
                    <Hidden smDown>
                        <Avatar variant='square' src='/images/Frame11.svg' sx={{ width: '100%', height: '15rem', mt: -25 }} />
                    </Hidden>
                </Stack>
            </Stack>
        </>
    )
}


