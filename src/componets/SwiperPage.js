// import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import { Avatar, Card, Stack, Box, Divider, Paper } from '@mui/material'
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import React, { useState } from 'react'
import { ruchiMockData } from '../util/mockdata';
import { Navigation } from 'swiper/modules';
import 'swiper/css/navigation';
import 'swiper/css/grid'



export const SwiperPage = () => {
    const [isLiked, setIsLiked] = useState(false);

    const handleLikeClick = () => {
        setIsLiked(!isLiked);
    };
    return (
        <Box  sx={{ p: 2, position: 'relative' }}>
            <Swiper
                navigation={true}
                modules={[Navigation]}
                spaceBetween={10}
                slidesPerView={4}
                onSlideChange={() => console.log('slide change')}
                onSwiper={(swiper) => console.log(swiper)}
                style={{ width: '98%', position: 'unset' }}
                // breakpoints={{
                //     640: {
                //         slidesPerView: 2,
                //         spaceBetween: 20,
                //     },
                //     768: {
                //         slidesPerView: 4,
                //         spaceBetween: 40,
                //     },
                //     1024: {
                //         slidesPerView: 5,
                //         spaceBetween: 50,
                //     },
                // }}
            >
                    <Box sx={{mx:10,bgcolor:'red'}}>

                {
                    ruchiMockData.map((each) => (
                        <SwiperSlide>
                            {/* <Paper elevation={2} sx={{ background: 'red', px: 5 }}> */}
                            <Card sx={{ borderRadius: 5, height: '358px', width: '100%' }}>
                                <Stack sx={{ p: 2 }}>
                                  <Stack direction={'row'} justifyContent={'flex-end'} width={'100%'}>
                                        <IconButton aria-label="like"
                                            onClick={handleLikeClick}
                                            sx={{ color: isLiked ? 'red' : 'inherit', }}
                                        >
                                            <FavoriteIcon />
                                        </IconButton>
                                    </Stack>
                                    <Stack justifyContent={'center'} alignItems={'center'} >
                                        <Avatar variant='square' src={each.img}
                                            sx={{ width: '108px', height: '100px', }}
                                        />
                                    </Stack>

                                    <Typography sx={{
                                        fontWeight: 'bold', whiteSpace: 'nowrap', width: '90%', overflow: 'hidden', textOverflow: 'ellipsis', mt: 2
                                    }}>
                                        {each.name}
                                    </Typography>
                                    <Typography sx={{ color: '#6F6F6F', fontSize: 14, }}>
                                        {each.weight}
                                    </Typography>
                                    <Stack direction={'row'} alignItems={'center'} mt={1}>
                                        <Box sx={{
                                            display: 'flex', flexDirection: 'row', bgcolor: '#1C9050', justifyContent: 'space-between',
                                            alignItems: 'center', width: '70px', height: '30px', px: 1.2, py: 0.9, borderRadius: 2
                                        }}>
                                            <Avatar variant='square' src='/images/Star.svg' sx={{ height: '14px', width: '14px' }} />
                                            <Divider orientation="vertical" sx={{ bgcolor: '#fff', fontWeight: 'bold' }} />
                                            <Typography sx={{ fontSize: 14, color: '#fff' }}>
                                                {each.ratings}
                                            </Typography>
                                        </Box>
                                        <Typography sx={{ ml: 1, color: '#6E6E6E', fontSize: 12 }}>
                                            {each.reviews}
                                        </Typography>
                                    </Stack>

                                    <Stack direction={'row'} alignItems={'center'} mt={1}>
                                        <Typography sx={{ fontWeight: 'bold', fontSize: 20, color: '#000000' }}>₹{each.price}</Typography>
                                        <Typography sx={{ textDecorationLine: 'line-through', color: '#6F6F6F', fontSize: 14, ml: 1 }}>₹{each.actualPrice}</Typography>
                                        <Typography sx={{ color: '#1C9050', fontSize: 16, ml: 1 }}>{each.discount}</Typography>
                                    </Stack>
 
                                </Stack>

                                <Button variant='contained' sx={{width:"100%"}} startIcon={<ShoppingCartIcon />}>
                                    Add to cart
                                </Button>
                            </Card>
                            {/* </Paper> */}
                        </SwiperSlide>

                    ))
                }
                    </Box>

            </Swiper>
        </Box>
    )
}
