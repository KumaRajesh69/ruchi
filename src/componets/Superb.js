import { Avatar, Button, Card, Grid, Stack, Typography } from '@mui/material'
import React from 'react'


const list = [
    {
        image: '/images/superb/Frame33.svg',
        // name: 'Basic Spices',
    },
    {
        image: '/images/superb/Frame25.svg',
    },
    {
        image: '/images/superb/Frame12.svg',
    },
    {
        image: '/images/superb/Frame13.svg',
    },

]
function Superb() {
    return (
        <>
            <Stack px={{ xs: 2, lg: 10 }} py={{ xs: 2, lg: 5 }}>
                <Grid container spacing={2} >
                    <Grid item xs={12} md={5} >
                        <Stack sx={{
                            position: 'relative',
                            zIndex: 0,
                        }}>
                            <Avatar variant='square' img src='/images/superb/Group1342.svg' sx={{ objectFit: 'cover', width: 'auto', height: '100%', borderRadius: 4 }} />
                            <Button sx={{
                                position: 'absolute',
                                zIndex: 1, mt: {xs:24,lg:40}, ml: {xs:2,lg:6}, bgcolor: '#3B160F', color: '#fff', textTransform: 'capitalize'
                            }} variant='contained'>
                                Explore Now
                            </Button>
                        </Stack>
                    </Grid>
                    <Grid item container xs={12} md={7} >
                        {
                            list.map((each) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <Stack sx={{ justifyContent: 'center', alignItems: 'center', }}>
                                            <Card sx={{ borderRadius: 4, m: 1 }}>

                                                <img src={each.image} style={{ width: '100%', height: 'auto' }} />
                                            </Card>
                                        </Stack>
                                    </Grid>
                                )
                            }
                            )
                        }

                    </Grid>
                </Grid>
            </Stack>
        </>
    )
}

export default Superb