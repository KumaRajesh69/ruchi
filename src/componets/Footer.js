import { Avatar, Divider, Grid, IconButton, Stack, Typography, useMediaQuery } from '@mui/material'
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import SportsBaseballIcon from '@mui/icons-material/SportsBaseball';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import theme from '../util/theme';
import React from 'react'
// import Desktop from './desktop';

const list = [

    {
        name: 'Address',
        obj1: 'Unit - 8 , DAV-OUAT Guest House Road, Gopabandhu Nagar, Adjacent to DAV Unit-8,Bhubaneswar - 751008',
    },
    {
        name: 'Company',
        obj1: 'About us',
        obj2: 'Careers',
        obj3: 'For partners',
        obj4: 'Teams',
        obj5: 'Privacy policy',
        obj6: 'Unsubscribe'
    },
    {
        name: 'Explore',
        obj1: 'Blogs',
        obj2: ' Spices',
        obj3: 'Sitemap',
        obj4: 'Exports'
    },
    {
        name: 'Quick links',
        obj1: ' Basic Spices',
        obj2: 'Whole spices ',
        obj3: 'Ready-Mix Spices',
        obj4: 'Pasta & Vernicilli',
    },
    {
        name: 'Contact us:',
        obj1: '(91) 9556266575',
        obj2: ' inquiries@gmail.com',

    }
]


function Footer() {


    const isLaptop = useMediaQuery('(min-width: 1025px)');

    return (
        <>
            <Stack alignItems={'center'} sx={{ bgcolor: '#191919',px: { xs: 2, lg: 5 }, }}>
                <Grid container sx={{  pt: 5, pb: 3, }}>
                    {list.map((each) => {
                        return (
                            <Grid item xs={12} lg={3} sx={{  mt: { xs: 3,  lg: 0 },textAlign:{xs:'center',lg:'start'} }} >
                                <Typography sx={{ fontSize: { xs: 20, lg: 25 }, fontWeight: 'bold', color: '#FFFFFF' }}>
                                    {each.name}
                                </Typography>
                                <Typography sx={{ width: {xs:'100%',lg:'50%'}, fontSize: { xs: 14, lg: 16 }, mt: 2, color: '#808080' }}>
                                    {each.obj1}
                                </Typography>
                                <Typography sx={{ fontSize: { xs: 14, lg: 16 }, color: '#808080', mt: 1, }}>
                                    {each.obj2}
                                </Typography>
                                <Typography sx={{ fontSize: { xs: 14, lg: 16 }, color: '#808080', mt: 1 }}>
                                    {each.obj3}
                                </Typography>
                                <Typography sx={{ fontSize: { xs: 14, lg: 16 }, color: '#808080', mt: 1 }}>
                                    {each.obj4}
                                </Typography>
                                <Typography sx={{ fontSize: { xs: 14, lg: 16 }, color: '#808080', mt: 1 }}>
                                    {each.obj5}
                                </Typography>
                                <Typography sx={{ fontSize: { xs: 14, lg: 16 }, color: '#808080', mt: 1 }}>
                                    {each.obj6}
                                </Typography>
                            </Grid>


                        )
                    })}

                </Grid>
                <Divider sx={{ bgcolor: '#808080', width: '100%' }} />
                {
                    isLaptop ?
                        <Stack direction={{ xs: 'column', lg: 'row' }} justifyContent={'space-between'} alignItems={'center'} width={'100%'} sx={{my:4,}}>
                            <Avatar variant='rounded' src='/images/appbar/ruchi.svg' sx={{ width: '120px', height: '80px' }} />
                            <Stack sx={{ml:20,mt:4}}>
                                <Stack direction={'row'} spacing={4} justifyContent={'center'}>
                                    <Typography sx={{ color: '#FFFF', }}>Terms</Typography>
                                    <Typography sx={{ color: '#FFFF', }}>Privacy</Typography>
                                    <Typography sx={{ color: '#FFFF', }}>Cookies</Typography>
                                </Stack>
                                <Typography sx={{ mt: {  lg: 6 }, fontSize: { lg: 15 }, color: '#FFFF', textAlign: 'center' }}>
                                    © 2022 All Rights Reserved. Designed by Smartters Studio
                                </Typography>
                            </Stack>
                            <Stack direction='row' spacing={3} sx={{ my: 2, }}>
                                <IconButton sx={{ border: 2, color: '#808080' }}>
                                    <YouTubeIcon sx={{ color: '#FFFF', fontSize: { xs: '20px', lg: '20px' } }} />
                                </IconButton>
                                <IconButton sx={{ border: 2, color: '#808080' }}>
                                    <InstagramIcon sx={{ color: '#FFFF', fontSize: { xs: '20px', lg: '20px' } }} />
                                </IconButton>
                                <IconButton sx={{ border: 2, color: '#808080' }}>
                                    <LinkedInIcon sx={{ color: '#FFFF', fontSize: { xs: '20px', lg: '20px' } }} />
                                </IconButton>
                                <IconButton sx={{ border: 2, color: '#808080' }}>
                                    <FacebookIcon sx={{ color: '#FFFF', fontSize: { xs: '20px', lg: '20px' } }} />
                                </IconButton>
                                <IconButton sx={{ border: 2, color: '#808080' }}>
                                    <TwitterIcon sx={{ color: '#FFFF', fontSize: { xs: '20px', lg: '20px' } }} />
                                </IconButton>
                            </Stack>
                        </Stack>
                        :
                        <Stack  justifyContent={'center'} alignItems={'center'} width={'100%'} padding={2}>
                            <Stack>
                                <Stack direction={'row'} justifyContent={'space-between'} >
                                    <Typography sx={{ color: '#808080', }}>Terms</Typography>
                                    <Typography sx={{ color: '#808080', }}>Privacy</Typography>
                                    <Typography sx={{ color: '#808080', }}>Cookies</Typography>
                                </Stack>
                            <Stack direction='row' justifyContent={'center'} spacing={3} sx={{ my: 2, }}>
                                <IconButton sx={{ border: 2, color: '#808080' ,fontSize:'20px'}}>
                                    <LinkedInIcon sx={{ color: '#FFFF', fontSize: { lg: '40px' } }} />
                                </IconButton>
                                <IconButton sx={{ border: 2, color: '#808080' }}>
                                    <FacebookIcon sx={{ color: '#FFFF', fontSize: { xs: '20px', lg: '20px' } }} />
                                </IconButton>
                                <IconButton sx={{ border: 2, color: '#808080' }}>
                                    <TwitterIcon sx={{ color: '#FFFF', fontSize: { xs: '20px', lg: '20px' } }} />
                                </IconButton>
                                </Stack>
                                <Typography sx={{ fontSize: { xs: 14, md: 16, lg: 12 }, color: '#808080', textAlign: 'center' }}>
                                    © 2022 All Rights Reserved. Designed by Smartters Studio
                                </Typography>
                            </Stack>
                            <Avatar variant='rounded' src='/images/appbar/ruchi.svg' sx={{ width: '120px', height: '80px',mt:2 }} />

                        </Stack>
                }

            </Stack>

        </>
    )
}

export default Footer