import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Avatar, Grid, Hidden, Stack } from '@mui/material';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import Paper from '@mui/material/Paper';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';


// function Appbar() {
//   return (
//     <div>
const drawerWidth = 240;
const navItems = ['Basic Spices', 'Whole Spices', 'Blended Spices', 'Ready-mix spices', 'Organic spices', 'Pasta & Vernicilli'];


function DrawerAppBar(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const container = window !== undefined ? () => window().document.body : undefined;

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center', color: '#07340CC2' }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        MUI
      </Typography>
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item} disablePadding >
            <ListItemButton sx={{ textAlign: 'center', }}>
              <ListItemText primary={item} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );


  return (
    <>
      <Stack width={'100%'}>
        <CssBaseline />
        <AppBar component="nav" position='relative'>
          <Hidden lgDown>
            <Stack sx={{
              width: '100%',
              direction: 'row',
              position: 'relative',
              zIndex: 0
            }}>
              <Avatar variant='square' src='/images/appbar/navbar.svg' sx={{ width: '100%', height: '65px' }} />
              <Stack sx={{
                width: '100%',
                justifyContent: 'space-between',
                direction: 'row', px: 2,
                position: 'absolute',
                zIndex: 1,
              }}>
                <Stack direction={'row'} sx={{
                  width: '100%',
                  pt: 1
                }}>
                  <Stack direction={'row'} spacing={3} sx={{
                    flex: 2,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                    px: 2,
                  }}>
                    <Avatar variant='rounded' src='/images/appbar/ruchi.svg' sx={{ width: '80px', height: '50px' ,ml:-8}} />

                    <Stack direction={'row'} >
                      <Paper component="form"
                        sx={{
                          p: '2px 4px',
                          display: 'flex',
                          alignItems: 'center',
                          width: 280, height: '45px',
                          borderRadius: '10px 0px 0px 10px'
                        }}>
                        <InputBase
                          sx={{ ml: 2, flex: 1 }}
                          placeholder="Search for products"
                          inputProps={{ 'aria-label': 'search google maps' }}
                        />
                      </Paper>
                      <Button variant='contained' sx={{
                        borderRadius: '1px 10px 10px 1px',
                        bgcolor: '#303030',
                        textTransform: 'capitalize',
                        width: 140
                      }}>
                        Search now
                      </Button>
                    </Stack>

                    <Button variant='contained' sx={{
                      borderRadius: 2,
                      fontSize: 12,
                      bgcolor: '#130F2640',
                      width: '17%',
                      height: '45px', textTransform: 'capitalize',
                      fontSize: 14
                    }}>
                      Download App
                    </Button>
                  </Stack>
                  <Stack spacing={3.5} direction={'row'} sx={{
                    flex: 1, width: '100%',
                    alignItems: 'center',
                  }}>
                    <Avatar variant='square' src='/images/appbar/bell.svg' sx={{ height: '20px', width: '20px' }} />
                    <Stack direction={'row'} sx={{
                      justifyContent: 'space-between',
                      alignItems: 'center'
                    }}>
                      <Avatar variant='square' src='/images/appbar/cart.svg' sx={{ height: '20px', width: '20px' }} />
                      Cart
                    </Stack>
                    <Stack direction={'row'}>
                      More
                      <KeyboardArrowDownIcon />
                    </Stack>
                    <Button variant='contained' sx={{ borderRadius: 3, bgcolor: '#F26147', height: '45px', textTransform: 'capitalize', width: '35%' }}>
                      Login/Signup
                    </Button>
                  </Stack>
                </Stack>
              </Stack>
            </Stack>
          </Hidden>

          <Toolbar>
            <Hidden smUp>

              <Stack direction={{ xs: 'row', lg: 'none' }} width={{ xs: '100%', lg: 'none' }} justifyContent={{ xs: 'space-between', lg: 'none' }}>
                <IconButton
                  color="inherit"
                  aria-label="open drawer"
                  edge="start"
                  onClick={handleDrawerToggle}
                  sx={{ mr: 2, display: { sm: 'none' } }}>
                  <MenuIcon />
                </IconButton>
                <img src='/images/appbar/ruchi.svg' style={{ marginBottom: -50, }} />
                <Stack direction={'row'} spacing={2}>
                  <img src='/images/appbar/bell.svg' sx={{ height: '20px', width: '20px' }} />
                  <img src='/images/appbar/cart.svg' sx={{ height: '20px', width: '20px' }} />
                </Stack>
              </Stack>
            </Hidden>

            <Box sx={{ display: { xs: 'none', sm: 'block', color: '#07340CC2' ,} }}>
              {navItems.map((item) => (
                <Button key={item} sx={{ color: '#fff', textTransform: 'capitalize', ml: 4 }}>
                  {item}
                </Button>
              ))}
            </Box>
          </Toolbar>

        </AppBar >
        <Box component="nav">
          <Drawer
            container={container}
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
            sx={{
              display: { xs: 'block', sm: 'none' },
              '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
            }}
          >
            {drawer}
          </Drawer>
        </Box>

      </Stack >
    </>
    // <Box sx={{ display: 'flex' }}>
    //   <CssBaseline />
    //   <AppBar component="nav">
    //     <Toolbar>
    //       <IconButton
    //         color="inherit"
    //         aria-label="open drawer"
    //         edge="start"
    //         onClick={handleDrawerToggle}
    //         sx={{ mr: 2, display: { sm: 'none' } }}
    //       >
    //         <MenuIcon />
    //       </IconButton>
    //       <Typography
    //         variant="h6"
    //         component="div"
    //         sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
    //       >
    //         MUI
    //       </Typography>
    //       <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
    //         {navItems.map((item) => (
    //           <Button key={item} sx={{ color: '#fff' }}>
    //             {item}
    //           </Button>
    //         ))}
    //       </Box>
    //     </Toolbar>
    //   </AppBar>
    //   <Box component="nav">
    //     <Drawer
    //       container={container}
    //       variant="temporary"
    //       open={mobileOpen}
    //       onClose={handleDrawerToggle}
    //       ModalProps={{
    //         keepMounted: true, // Better open performance on mobile.
    //       }}
    //       sx={{
    //         display: { xs: 'block', sm: 'none' },
    //         '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
    //       }}
    //     >
    //       {drawer}
    //     </Drawer>
    //   </Box>

    // </Box>
  );
}


export default DrawerAppBar;
{/* </div>
  )
}

export default Appbar */}