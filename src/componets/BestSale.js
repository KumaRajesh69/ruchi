// import React from 'react'
import { Grid, Stack, Typography, Button, Avatar, Hidden } from '@mui/material'
// import React from 'react'
import React, { useState } from 'react';
import { ArrowBack, ArrowForward } from '@mui/icons-material';
import { SwiperPage } from './SwiperPage';
import { Organic } from './organic';

function BestSale() {
    return (
        <>


            <Stack sx={{
                // backgroundImage: 'url("/images/Frame11.svg")',
                // backgroundSize: 'cover',
                // backgroundPosition: 'center',
                // width: '100%',
                // height: '30vh',
                // py: 4,
                // flexDirection:'column-reverse',
                mt: { xs: 0, lg: -10 }
            }}>
                <Stack sx={{ height: 'auto' }}>
                    <Grid container spacing={2} sx={{ zIndex: 1 }}>

                        <Grid item lg={4} sx={{ zIndex: { xs: 1, lg: 'none' } }}>
                            <Stack ml={{ xs: 0, lg: 4 }}>
                                <Stack direction={'row'} mt={{xs:0,lg:4}}>
                                    <Typography variant='h6' sx={{
                                        fontSize: { xs: '100px', lg: '110px' },
                                        fontWeight: '900', color: '#0A6510',
                                        fontFamily: 'Playfair Display',
                                        mt: 0.5,ml:2
                                    }}>
                                        Best
                                    </Typography>
                                    <Button variant='contained' sx={{
                                        borderRadius: {xs:4,lg:2}, bgcolor: '#FFE944', color: '#000', mt: { xs: 7, lg: 10 }, ml: {xs:2,lg:5},
                                        textTransform: 'capitalize', height: 35, width: 80, fontSize: 13
                                    }}>
                                        View all
                                    </Button>
                                </Stack>

                                <Typography fontWeight="100" fontSize={{ xs: '75px', lg: '90px' }}
                                    sx={{ color: '#FFE944', fontFamily: 'Playfair Display', mt: { xs: -12, lg: -12.4 }, ml: { xs: 6, lg: 8 } }}>
                                    sellers
                                </Typography>
                                <Hidden smDown>
                                    <Typography sx={{ fontSize: 20, width: '80%', color: '#fff', mt: -2 }}>
                                    Explore our some of the Bestseller packages.
                                </Typography>
                                </Hidden>
                                
                            </Stack>
                        </Grid>
                        <Hidden lgUp>
                            <Avatar variant='square' src='/images/Frame11.svg' sx={{
                                width: '100%', height: '15rem',
                                mt: -7, zIndex: 0
                            }} />
                        </Hidden>
                        <Grid item lg={8} sx={{ zIndex: { xs: 1, lg: 0 }, mt: { xs: -25, lg: 0 } }}>
                            <Organic />
                        </Grid>
                    </Grid>
                    <Hidden smDown>
                        <Avatar variant='square' src='/images/Frame11.svg' sx={{ width: '100%', height: '12rem', mt: -21 }} />
                    </Hidden>
                </Stack>
            </Stack>
        </>
    )
}

export default BestSale