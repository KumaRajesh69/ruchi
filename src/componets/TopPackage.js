import { Typography, Button, Stack, Grid, Hidden } from '@mui/material'
// import React from 'react'
import React, { useState } from 'react';
import { Organic } from './organic';



export const TopPackage = () => {
    // const [sliderValue, setSliderValue] = useState(50);
    // const handleSliderChange = (event, newValue) => {
    //     setSliderValue(newValue);
    // };
    // const handleLeftButtonClick = () => {
    //     setSliderValue((prevValue) => prevValue - 10); // Decrease the value by 10 on left arrow click
    // };
    // const handleRightButtonClick = () => {
    //     setSliderValue((prevValue) => prevValue + 10); // Increase the value by 10 on right arrow click
    //     console.log('Button clicked!');
    // };


    return (
        <>
            <Stack sx={{
                backgroundImage: 'url("/images/toppackage/Frame9.svg")',
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                width: '100%',
                height: 'auto',
                py: 4
            }}>
                <Grid container spacing={2} sx={{ display: 'flex', flexDirection: { xs: 'column-reverse', lg: 'row' } }}>
                    <Grid item lg={8} >
                        {/* <SwiperPage /> */}
                        <Organic />
                    </Grid>
                    <Grid item lg={4}>
                        <Stack px={{xs:1.2,lg:0}}>
                            <Stack direction={{ xs: 'row', lg: 'column' }}>
                                <Typography variant='h6' sx={{ fontSize: { xs: '100px', lg: '127px' }, fontWeight: '900', color: '#FFFFFF', fontFamily: 'Playfair Display', mt: -3 }}>
                                    Top
                                </Typography>
                                <Hidden lgUp>
                                    <Button variant='contained' sx={{
                                        borderRadius: {xs:4,lg:2}, bgcolor: '#FFE944', color: '#000', mt: 2,
                                        textTransform: 'capitalize', height: 35, width: 80, fontSize: 13, mt: { xs: 6, lg: 10 }, ml: {xs:6,lg:3},
                                    }}>
                                        View all
                                    </Button>
                                </Hidden>
                            </Stack>

                            <Typography fontWeight="100" sx={{
                                fontSize: { xs: '60px', lg: '90px' },
                                color: '#FFE944', fontFamily: 'Playfair Display', mt: { xs: -9, lg: -12 }, ml: { xs: 2.2, lg: 1.7 }
                            }}>
                                Packages
                            </Typography>
                            <Typography sx={{ fontSize: { xs: 14, lg: 20 }, width: { xs: '100%', lg: '80%' }, color: '#fff', mt: { xs: 2, lg: -2 } }}>
                                Explore our some of the Bestseller packages.
                            </Typography>
                            <Hidden smDown>
                                <Button variant='contained' sx={{
                                    borderRadius: 2, bgcolor: '#FFE944', color: '#000', mt: 2,
                                    textTransform: 'capitalize', height: 35, width: 90, 
                                }}>
                                    View all
                                </Button>
                            </Hidden>
                        </Stack>
                    </Grid>
                </Grid>
            </Stack>



            {/* <Stack>
                    <IconButton onClick={()=>handleLeftButtonClick()}>
                        <ArrowBack />
                    </IconButton>
                    <Slider
                        value={()=>sliderValue()}
                        onChange={handleSliderChange}
                        min={0}
                        max={100}
                        step={1}
                        style={{ width: 300 }}
                    />
                    <IconButton onClick={()=>handleRightButtonClick()}>
                        <ArrowForward />
                    </IconButton>
                </Stack> */}
        </>
    )
}

