// import { Stack, Typography } from '@mui/material'
// import React from 'react'
// import { Organic } from './organic'
import { Avatar, Fab, Stack, Box, Divider, Paper } from '@mui/material'
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import React, { useRef, useState } from 'react'
import { ruchiMockData } from '../util/mockdata';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@mui/icons-material';


export const SuggestItem = () => {

    const [isLiked, setIsLiked] = useState(false);

    const scrollRef = useRef();

    const handleLikeClick = () => {
        setIsLiked(!isLiked);
    };

    const scrollRight = () => {
        console.log('Scrol ref ---> ', scrollRef);
        scrollRef.current.scrollLeft += 200;
    }
    const scrollLeft = () => {
        console.log('Scrol ref ---> ', scrollRef);
        scrollRef.current.scrollLeft -= 200;
    }

    return (
        <>
            <Stack justifyContent={{xs:'flex-start',lg:'center'}} alignItems={{xs:'flex-start',lg:'center'}} my={{xs:2,lg:0}}>
                <Typography sx={{ fontWeight: 'bold', fontSize: {xs:15,lg:19} }}>Suggested Items</Typography>
                <Typography sx={{fontWeight: {xs:'none',lg:'bold'},  fontSize:  {xs:13,lg:15}}}>Suggested items as per your activity</Typography>

                <Stack width={'100%'} mt={2}>
                    <Stack direction={'row'} sx={{ alignItems: 'center', width: '100%' }}>
                        <Stack justifyContent={'center'} sx={{ width: { xs: '100px', lg: '170px' }, pl: { xs: '50px', lg: '120px' }, }}>
                            <Fab color="primary" aria-label="add" size='small' sx={{ zIndex: 1000, display: { lg: 'flex', xs: 'none' } }}
                                onClick={scrollLeft}
                            >
                                <KeyboardArrowLeft />
                            </Fab>
                        </Stack>

                        <Stack direction={'row'} sx={{
                            width: { lg: '90vw', md: '65vw', sm: '100vw', xs: '100vw' },
                            overflowX: 'auto',
                            scrollBehavior: 'smooth', ml: '-100px',
                            '::-webkit-scrollbar': {
                                display: 'none'
                            },
                            '-ms-overflow-style': 'none',
                            'scrollbar-width': 'none',
                            // bgcolor:'red'
                        }}
                            ref={scrollRef}
                        >
                            {
                                ruchiMockData.map((each) => (
                                    <Paper sx={{ width: '250px', borderRadius: 3, mr: 2 }}>
                                        <Stack sx={{ px: 2 }}>
                                            <Stack direction={'row'} justifyContent={'flex-end'} width={'100%'} mt={1}>
                                                <IconButton aria-label="like"
                                                    onClick={handleLikeClick}
                                                    sx={{ color: isLiked ? 'red' : 'inherit', }}
                                                >
                                                    <FavoriteIcon />
                                                </IconButton>
                                            </Stack>
                                            <Stack justifyContent={'center'} alignItems={'center'} >
                                                <Avatar variant='square' src={each.img}
                                                    sx={{ width: '108px', height: '100px', }}
                                                />
                                            </Stack>

                                            <Typography sx={{
                                                fontWeight: 'bold', whiteSpace: 'nowrap', width: '90%',
                                                overflow: 'hidden', textOverflow: 'ellipsis', mt: 2
                                            }}>
                                                {each.name}
                                            </Typography>
                                            <Typography sx={{ color: '#6F6F6F', fontSize: 14, mt: 1 }}>
                                                {each.weight}
                                            </Typography>
                                            <Stack direction={'row'} alignItems={'center'} mt={1}>
                                                <Box sx={{
                                                    display: 'flex', flexDirection: 'row', bgcolor: '#1C9050', justifyContent: 'space-between',
                                                    alignItems: 'center', width: '70px', height: '30px', px: 1.2, py: 0.9, borderRadius: 2
                                                }}>
                                                    <Avatar variant='square' src='/images/Star.svg' sx={{ height: '14px', width: '14px' }} />
                                                    <Divider orientation="vertical" sx={{ bgcolor: '#fff', fontWeight: 'bold' }} />
                                                    <Typography sx={{ fontSize: 14, color: '#fff' }}>
                                                        {each.ratings}
                                                    </Typography>
                                                </Box>
                                                <Typography sx={{ ml: 1, color: '#6E6E6E', fontSize: 12 }}>
                                                    {each.reviews}
                                                </Typography>
                                            </Stack>
                                            <Stack direction={'row'} alignItems={'center'} mt={1}>
                                                <Typography sx={{ fontWeight: 'bold', fontSize: 20, color: '#000000' }}>
                                                    ₹{each.price}
                                                </Typography>
                                                <Typography sx={{ textDecorationLine: 'line-through', color: '#6F6F6F', fontSize: 14, ml: 1 }}>
                                                    ₹{each.actualPrice}
                                                </Typography>
                                                <Typography sx={{ color: '#1C9050', fontSize: 16, ml: 1 }}>{each.discount}</Typography>
                                            </Stack>
                                        </Stack>
                                        <Stack width={'100%'} mt={1} >
                                            <Button variant='contained' sx={{ bgcolor: 'black', borderRadius: '0px 0px 12px 12px' }}>
                                                <ShoppingCartIcon />
                                                Add to cart
                                            </Button>
                                        </Stack>
                                    </Paper>
                                ))
                            }
                        </Stack>
                        <Fab color="primary" aria-label="add" size='small' sx={{ zIndex: 1000, ml: '-100px', display: { lg: 'flex', xs: 'none' } }}
                            onClick={scrollRight}
                        >
                            <KeyboardArrowRight />
                        </Fab>

                    </Stack>
                </Stack>
            </Stack>
        </>
    )
}
