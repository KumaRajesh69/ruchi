export const ruchiMockData = [
    {
        img:'/images/ruchi/ruchi-img.svg',
        name:'Ruchi CHILLI POWDER 5...',
        weight:'500gm',
        ratings:4.9,
        reviews:300,
        price:120,
        actualPrice:500,
        discount:'40% off',
    },
    {
        img:'/images/ruchi/ruchi-img.svg',
        name:'Ruchi CHILLI POWDER 5...',
        weight:'500gm',
        ratings:4.9,
        reviews:300,
        price:100,
        actualPrice:500,
        discount:'40% off',
    },
    {
        img:'/images/ruchi/ruchi-img.svg',
        name:'Ruchi CHILLI POWDER 5...',
        weight:'500gm',
        ratings:4.9,
        reviews:300,
        price:360,
        actualPrice:500,
        discount:'40% off',
    },
    {
        img:'/images/ruchi/ruchi-img.svg',
        name:'Ruchi CHILLI POWDER 5...',
        weight:'500gm',
        ratings:4.9,
        reviews:300,
        price:120,
        actualPrice:500,
        discount:'40% off',
    },
    {
        img:'/images/ruchi/ruchi-img.svg',
        name:'Ruchi CHILLI POWDER 5...',
        weight:'500gm',
        ratings:4.9,
        reviews:300,
        price:120,
        actualPrice:500,
        discount:'40% off',
    },
    {
        img:'/images/ruchi/ruchi-img.svg',
        name:'Ruchi CHILLI POWDER 5...',
        weight:'500gm',
        ratings:4.9,
        reviews:300,
        price:120,
        actualPrice:500,
        discount:'40% off',
    },
    {
        img:'/images/ruchi/ruchi-img.svg',
        name:'Ruchi CHILLI POWDER 5...',
        weight:'500gm',
        ratings:4.9,
        reviews:300,
        price:120,
        actualPrice:500,
        discount:'40% off',
    },
    {
        img:'/images/ruchi/ruchi-img.svg',
        name:'Ruchi CHILLI POWDER 5...',
        weight:'500gm',
        ratings:4.9,
        reviews:300,
        price:120,
        actualPrice:500,
        discount:'40% off',
    }
]