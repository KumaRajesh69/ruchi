import { createTheme } from '@mui/material/styles';
import { red } from '@mui/material/colors';

// Create a theme instance.
const theme = createTheme({
    palette: {
        primary: {
            main: '#07340CC2',
        },
        secondary: {
            main: '#556cd6',
        },
        error: {
            main: red.A400,
        },
        background: {
            default: '#ffffff'
        }
    },
    
    typography: {
        fontFamily: "Open Sans,sans-serif",
    }
});

export default theme;
