import { CssBaseline, Hidden, Stack } from '@mui/material'
import React from 'react'
import Appbar from '../src/componets/Appbar'
import RuchiCard from '../src/componets/RuchiCard'
import ZeroHour from '../src/componets/ZeroHour'
import Footer from '../src/componets/Footer'
import AboutRuchi from '../src/componets/AboutRuchi'
import Brand from '../src/componets/Brand'
import Quality from '../src/componets/Quality'
import YearOfRuch from '../src/componets/YearOfRuch'
import Category from '../src/componets/Category'
import Superb from '../src/componets/Superb'
import RuchiApp from '../src/componets/RuchiApp'
import { Organic } from '../src/componets/organic'
import { Refer } from '../src/componets/Refer'
import { BeReady } from '../src/componets/BeReady'
import PartyZone from '../src/componets/BestSale'
import { SwiperPage } from '../src/componets/SwiperPage'
import { TopPackage } from '../src/componets/TopPackage'
import SliderSection from '../src/componets/Slider'
import DisplayOffer from '../src/componets/DisplayOffer'
import BestSale from '../src/componets/BestSale'
import { BasicSpices } from '../src/componets/BasicSpices'
import { OrganicSpices } from '../src/componets/OrganicSpices'
import { WholeSpices } from '../src/componets/WholeSpices'
import { SuggestItem } from '../src/componets/SuggestItem'



function index() {

  return (
    <>
      <CssBaseline />
      <Appbar />
      <DisplayOffer />
      <ZeroHour />
      <Category />
      <Superb />
      <TopPackage />
      <BeReady />
      <BestSale />
      <Refer />
      <SuggestItem />
      <YearOfRuch />
      <Quality />
      <BasicSpices />
      <WholeSpices />
      <OrganicSpices />
      <RuchiApp />
      <Hidden lgDown>
        <Brand />
        <AboutRuchi />
      </Hidden>

      <Footer />


      {/* <RuchiCard /> */}
      {/* <Organic /> */}
      {/* <PartyZone /> */}
      {/* <SliderSection /> */}
      {/* <SwiperPage /> */}

    </>
  )
}

export default index


